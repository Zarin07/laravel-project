<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        // echo 'I am from controller';
        return view('products.index');
    }

    public function show($id){
        // dd($id);
        $productName = 'T-shirt';
        return view('products.show', compact('id', 'productName'));
    }
}
