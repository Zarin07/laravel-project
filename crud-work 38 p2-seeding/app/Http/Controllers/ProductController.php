<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
    //   $products = Product::all();
    $products = Product::orderBy('id', 'desc')->get();
    //    dd($products);
        return view('backend.products.index', compact('products'));
    }


    public function create(){

        return view('backend.products.create');
    }

    public function store(){
        try{
            $postData = request()->all();
            Product::create([
                'title' => $postData['title'],
                'price' => $postData['price']
            ]);
            
            return redirect()->route('products.index');

        }catch(QueryException $e){
            dd($e->getMessage());
        }

       
    }

    public function show($id){
        // dd($id);
        $productName = 'T-shirt';
        return view('products.show', compact('id', 'productName'));
    }
}
