<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






Route::get('/', [FrontendController::class, 'welcome']);

Route::get('/admin/products', [ProductController::class, 'index'])->name('products.index');

Route::get('/admin/products/create', [ProductController::class, 'create'])->name('products.create');
Route::post('/admin/products/store', [ProductController::class, 'store'])->name('products.store');


Route::get('/admin', function () {
    return view('backend.dashboard');
});


Route::get('/table', function () {
    return view('backend.table');
});


