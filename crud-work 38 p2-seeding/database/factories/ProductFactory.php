<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    public function definition()
    { $path = 'public/storage/products';
        return [
            'title' => $this->faker->sentence(),
            'description' => $this->faker->realText(500),
            'price' => $this->faker->numberBetween(100, 5000),
            'image' => $this->faker->image($path, 250, 300) //If image is not auto generated here, faker is not working dont worry about your codes
        ];
    }

  
}
