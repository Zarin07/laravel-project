<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Database\Factories\ProductFactory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        echo 'Running database seeder';

        // DB::table('categories')->insert([
        //     'title' => 'Fashion',
        //     'description' => 'This is test description for fashion'
        // ]);


        //   Category::create([
        //     'title' => 'Footware',
        //        'description' => 'This is test description '
        //   ]);


        // User::factory(10)->create();

        // Product::factory(100)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);




        $this->call([
            ProductsTableSeeder::class
        ]);
    }
}
